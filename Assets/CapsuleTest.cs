﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CapsuleTest : MonoBehaviour
{
    private Rigidbody rigidBody;
    public Text debugText;

    // Start is called before the first frame update
    void Start()
    {
        rigidBody=GetComponent<Rigidbody>();
        rigidBody.AddRelativeForce(new Vector3(1,0,0),ForceMode.VelocityChange);
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        Vector3 controlForce = new Vector3(0,0.2f,0);
        rigidBody.AddRelativeForce(controlForce, ForceMode.Force);
        //rigidBody.AddRelativeTorque(new Vector3(0,0,0.1f), ForceMode.Force);
        
        if (rigidBody.velocity.magnitude>0.01f) {

            Vector3 up=transform.up;
            Vector3 directionOfMotion=rigidBody.velocity.normalized;
            Quaternion rot=Quaternion.LookRotation(directionOfMotion,up);
            //transform.rotation = rot; //Vector3.Slerp(transform.forward,directionOfMotion,0.1f);
            transform.rotation = Quaternion.Slerp(transform.rotation,rot,0.1f);
        }

        Vector3 f=transform.forward;
        Vector3 u=transform.up;
        //debugText.text=f.x.ToString("0.00")+" "+f.y.ToString("0.00")+" "+f.z.ToString("0.00")+"   "+
        //u.x.ToString("0.00")+" "+u.y.ToString("0.00")+" "+u.z.ToString("0.00");
        

    }
}
