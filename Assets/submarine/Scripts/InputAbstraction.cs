﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class InputAbstraction : MonoBehaviour
{
	public InputAction fireAction;
	public InputAction movementAction;
	public InputAction accelerateAction;
	public InputAction cameraPositionAction;

    private bool fireNow;
    private bool fired;

	void Start() {
		fireAction.performed += ctx => Fire(ctx);
	}
	void OnEnable()	{
		fireAction.Enable();
		movementAction.Enable();
		accelerateAction.Enable();
		cameraPositionAction.Enable();
	}

    void Fire(InputAction.CallbackContext ctx)
    {   
        fireNow=true;
        fired=false;
    }

    public bool getFire() {
        bool result=false;
        if (fireNow && !fired) {
            result=true;
            fired=true;
        }
        return result;
    }

	public float getAcceleration() {
		return accelerateAction.ReadValue<float>();
    }
	public float getYMovement() {
		return movementAction.ReadValue<Vector2>().y;
	}

    public float getXMovement() {
		return movementAction.ReadValue<Vector2>().x;
	}

    public float getXCameraRotate() {
        return cameraPositionAction.ReadValue<Vector2>().x;
    }
}
