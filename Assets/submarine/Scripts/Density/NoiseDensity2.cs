﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NoiseDensity2 : DensityGenerator {

    [Header ("Micro Noise")]

    public int numOctaves = 4;
    public float lacunarity = 2;
    public float persistence = .5f;
    public float noiseScale = 1;
    //public float noiseWeight = 1;

    [Header ("Macro Noise")]
    public int numOctaves_macro = 4;
    public float lacunarity_macro = 2;
    public float persistence_macro = .5f;
    public float noiseScale_macro = 1;
    public float noiseWeight_macro = 0;

    [Header ("Vertical")]

    public float bedrockAltitude = 0;
    public float bedrockFlatten=0.05f;
    public float groundAltitude=20;
    public float groundVariation=20;
    
    public float groundFlatten=0.01f;

    [Header ("General Settings")]
    public int seed;
    public bool closeEdges;
    //public float floorOffset = 1;
    //public float weightMultiplier = 1;

    //public float hardFloorHeight;
    //public float hardFloorWeight;

    //public Vector4 shaderParams;

    public override ComputeBuffer Generate (ComputeBuffer pointsBuffer, int numPointsPerAxis, float boundsSize, Vector3 worldBounds, Vector3 centre, Vector3 offset, float spacing) {
        buffersToRelease = new List<ComputeBuffer> ();

        // Noise parameters
        var prng = new System.Random (seed);
        var offsets = new Vector3[numOctaves];
        float offsetRange = 1000;
        int octaveMax=numOctaves>numOctaves_macro?numOctaves:numOctaves_macro;
        for (int i = 0; i < octaveMax; i++) {
            offsets[i] = new Vector3 ((float) prng.NextDouble () * 2 - 1, (float) prng.NextDouble () * 2 - 1, (float) prng.NextDouble () * 2 - 1) * offsetRange;
        }

        var offsetsBuffer = new ComputeBuffer (offsets.Length, sizeof (float) * 3);
        offsetsBuffer.SetData (offsets);
        buffersToRelease.Add (offsetsBuffer);

        densityShader.SetVector ("centre", new Vector4 (centre.x, centre.y, centre.z));
        densityShader.SetBool ("closeEdges", closeEdges);
        densityShader.SetBuffer (0, "offsets", offsetsBuffer);

        densityShader.SetInt ("octaves", Mathf.Max (1, numOctaves));
        densityShader.SetFloat ("lacunarity", lacunarity);
        densityShader.SetFloat ("persistence", persistence);
        densityShader.SetFloat ("noiseScale", noiseScale);

        densityShader.SetInt ("octaves_macro", Mathf.Max (1, numOctaves_macro));
        densityShader.SetFloat ("lacunarity_macro", lacunarity_macro);
        densityShader.SetFloat ("persistence_macro", persistence_macro);
        densityShader.SetFloat ("noiseScale_macro", noiseScale_macro);
        densityShader.SetFloat ("noiseWeight_macro", noiseWeight_macro);

        densityShader.SetFloat ("bedrockAltitude", bedrockAltitude);
        densityShader.SetFloat ("bedrockFlatten", bedrockFlatten);

        densityShader.SetFloat ("groundAltitude", groundAltitude);
        densityShader.SetFloat ("groundVariation", groundVariation);
        densityShader.SetFloat ("groundFlatten", groundFlatten);
        
        return base.Generate (pointsBuffer, numPointsPerAxis, boundsSize, worldBounds, centre, offset, spacing);
    }
}