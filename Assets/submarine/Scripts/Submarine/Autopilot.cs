using System.Collections;
using System.Collections.Generic;
using System;
using System.Diagnostics;
using UnityEngine;

public class Autopilot
{
	public float clearanceRadius=0.8f;
	public float visionDistance=32f;
	public float visionFOV=22.5f;

	private MeshGenerator densityMesh;
	private Transform t2;

	private float densityTurnHorizontal;
	private float densityTurnVertical;

	//private const float OOB_DENSITY=-999;

	public bool outOfBounds=false;
	public bool impact=false;

	public String debugString;

	private const int LAYER_MASK=(1<<10); //just cave walls
	//~(1<<9); //all except bullets

	public Autopilot(Transform transform, MeshGenerator densityMesh=null) {
		this.densityMesh=densityMesh;
		this.t2=transform;
	}

	public float getTurnHorizontal() {
		return densityTurnHorizontal;
	}

	public float getTurnVertical() {
		return densityTurnVertical;
	}

	public bool isOutOfBounds() {
		return outOfBounds;
	}

	public bool isImpact() {
		return impact;
	}

	public void restart() {
		outOfBounds=false;
	}

	public void process() {

		if (outOfBounds) return;

		if (densityMesh!=null && !densityMesh.OnMesh(t2.position))
		{
			if (!outOfBounds) {
				outOfBounds=true;

			}
		}

		RaycastHit hit;
		bool allowPitchLeveling=true;
		float pitchDegrees=t2.localEulerAngles.x;
        if (pitchDegrees>180) pitchDegrees-=360.0f;

		if (Physics.SphereCast(t2.position, clearanceRadius, t2.forward, out hit, visionDistance, LAYER_MASK))
		{
			float dCenter=hit.distance;

			if (dCenter<clearanceRadius) {
				densityTurnHorizontal=0f;
				densityTurnVertical=0f;
				impact=true;
			}
			else
			{	impact=false;
			}

			float beta=visionFOV/360f*Mathf.PI;
			float b0=Mathf.Sin(beta);
			float b1=Mathf.Cos(beta);

			float dLeft=visionDistance;
			float dRight=visionDistance;
			float dUp=visionDistance;
			float dDown=visionDistance;
			Vector3 vLeft=new Vector3(-b0,0,b1);
			Vector3 vRight=new Vector3(+b0,0,b1);
			Vector3 vUp=new Vector3(0,+b0,b1);
			Vector3 vDown=new Vector3(0,-b0,b1);

			if (Physics.Raycast(t2.position, t2.TransformDirection(vLeft), out hit, visionDistance, LAYER_MASK))
        	{	dLeft=hit.distance;
			}
			if (Physics.Raycast(t2.position, t2.TransformDirection(vRight), out hit, visionDistance, LAYER_MASK))
        	{	dRight=hit.distance;
			}
			if (Physics.Raycast(t2.position, t2.TransformDirection(vUp), out hit, visionDistance, LAYER_MASK))
        	{	dUp=hit.distance;
			}
			if (Physics.Raycast(t2.position, t2.TransformDirection(vDown), out hit, visionDistance, LAYER_MASK))
        	{	dDown=hit.distance;
			}

			
			Vector3 vClearance=t2.TransformDirection(Vector3.left)*clearanceRadius;
			UnityEngine.Debug.DrawRay(t2.position+vClearance, t2.TransformDirection(Vector3.forward) * visionDistance, Color.blue);
			UnityEngine.Debug.DrawRay(t2.position-vClearance, t2.TransformDirection(Vector3.forward) * visionDistance, Color.blue);
			UnityEngine.Debug.DrawRay(t2.position, t2.TransformDirection(vLeft) * visionDistance, Color.blue);
            UnityEngine.Debug.DrawRay(t2.position, t2.TransformDirection(vRight) * visionDistance, Color.blue);
			
			//UnityEngine.Debug.DrawRay(t2.position, t2.TransformDirection(vUp) * visionDistance, Color.blue);
			//UnityEngine.Debug.DrawRay(t2.position, t2.TransformDirection(vDown) * visionDistance, Color.blue);
            
			float f=(visionDistance-dCenter)/visionDistance;
			float pitchMax=45f;
			
            if (pitchDegrees<-pitchMax) {
                //too much up already
                dUp=0.0f;
            }
            else if (pitchDegrees>pitchMax) {
                //too much down already
                dDown=0.0f;
            }
			

			String dir="forward";
			/*
			float t=5.0f;
            if (dCenter<t )
            {   //underground - need to come up
                 densityTurnVertical=1.0f;
                 allowPitchLeveling=false;
            }
            else
			*/
            {
                //debugText.text="normal";
				float dCenterPlusRadius=dCenter+clearanceRadius; // to compensate for spherecast 
                if (dLeft>=dCenterPlusRadius && dLeft>=dRight && dLeft>=dUp && dLeft>=dDown)
                {   densityTurnHorizontal=-f;
					dir="left";
                }
                else if (dRight>=dCenterPlusRadius && dRight>=dLeft && dRight>=dUp && dRight>=dDown)
                {   densityTurnHorizontal=+f;
					dir="right";
                }
                else if (dDown>=dCenterPlusRadius && dDown>=dLeft && dDown>=dUp && dDown>=dRight)
                {   densityTurnVertical=-f;
                    allowPitchLeveling=false;
					dir="down";
                }
                else if (dUp>=dCenterPlusRadius && dUp>=dLeft && dUp>=dRight && dUp>=dDown)
                {   densityTurnVertical=+f;
                    allowPitchLeveling=false;
					dir="up";
                }
				else
				{
					//we are blocked but forward looks the best - choose second best
					dir="blocked";
					allowPitchLeveling=false;
				}
            }

			//debugString=dir+" "+pitchDegrees.ToString("0.0")+" "+dLeft.ToString("0.0")+" "+dCenter.ToString("0.0")+" "+dRight.ToString("0.0")+" "+dUp.ToString("0.0")+" "+dDown.ToString("0.0");
			debugString=dir+" "+dCenter.ToString("0.0");
			if (impact) {
				debugString+=" IMPACT";
			}
			if (outOfBounds) {
				debugString+=" OOB";
			}
		
		}
		else
		{	
			densityTurnHorizontal*=0.25f;
        	densityTurnVertical*=0.25f;

			debugString="clear";
		}

		if (allowPitchLeveling)
        {	//dampen pitch
            densityTurnVertical=pitchDegrees*0.001f;
        }
    }

	/*
	private static long nanoTime() {
		long nano = 10000L * Stopwatch.GetTimestamp();
		nano /= TimeSpan.TicksPerMillisecond;
		nano *= 100L;
		return nano;
	}
	*/


}