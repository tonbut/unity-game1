﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

public class Submarine : MonoBehaviour {

    [Header ("Autopilot")]
    public float densityThreshold = 3.0f;

    [Header ("General")]
    public MeshGenerator densityMesh;
    public float maxSpeed = 5;
    public float maxPitchSpeed = 3;
    public float maxTurnSpeed = 50;
    public float acceleration = 2;

    public float smoothSpeed = 3;
    public float smoothTurnSpeed = 3;

    public Transform physicalContainer;
    public Transform propeller;
    public Transform rudderPitch;
    public Transform rudderYaw;
    public float propellerSpeedFac = 2;
    public float rudderAngle = 30;

    Vector3 velocity;
    float yawVelocity;
    float pitchVelocity;
    float currentSpeed;
    public Material propSpinMat;

    public float pitchMax=60.0f;


    float densityTurnHorizontal=0;
    float densityTurnVertical=0;
    //float avgDensity=0;

    public Transform debugPoint1;
    public Transform debugPoint2;

    public Transform debugPoint3;

    public Transform debugPoint4;

    public Text debugText;

    private float distanceToWall;

    private Autopilot autopilot;

    void Start () {
        currentSpeed = 0; //maxSpeed*0.8f;
        autopilot=new Autopilot(transform,densityMesh);
    }

    void Update () {

        if (densityMesh??false) {
            autopilot.process();
            
            //densityTurnHorizontal=autopilot.getTurnHorizontal();
            //densityTurnVertical=autopilot.getTurnVertical();
            if (autopilot.isOutOfBounds()) {
			    autopilot.restart();
            }
		}

        float accelDir = 0;
        if (Input.GetKey (KeyCode.Q)) {
            accelDir -= 1;
        }
        if (Input.GetKey (KeyCode.E)) {
            accelDir += 1;
        }
        /*
        if (avgDensity>2.0f && currentSpeed>maxSpeed*0.25f)
        {  accelDir -= 1; }

        if (avgDensity<2f && currentSpeed<maxSpeed)
        {  accelDir += 0.5f; }
        */


        currentSpeed += acceleration * Time.deltaTime * accelDir;
        currentSpeed = Mathf.Clamp (currentSpeed, 0, maxSpeed);
        float speedPercent = currentSpeed / maxSpeed;

        Vector3 targetVelocity = transform.forward * currentSpeed;
        velocity = Vector3.Lerp (velocity, targetVelocity, Time.deltaTime * smoothSpeed);

        float v= densityTurnVertical*1.0f + Input.GetAxisRaw ("Vertical")*0.25f ;
        float targetPitchVelocity = v * maxPitchSpeed;
        pitchVelocity = Mathf.Lerp (pitchVelocity, targetPitchVelocity, Time.deltaTime * smoothTurnSpeed);
        
        float h=densityTurnHorizontal*1.0f + Input.GetAxisRaw ("Horizontal")*0.25f;
        float targetYawVelocity = h * maxTurnSpeed;
        yawVelocity = Mathf.Lerp (yawVelocity, targetYawVelocity, Time.deltaTime * smoothTurnSpeed);

        transform.localEulerAngles += (Vector3.up * yawVelocity + Vector3.left * pitchVelocity) * Time.deltaTime * speedPercent;
        transform.Translate (transform.forward * currentSpeed * Time.deltaTime, Space.World);

        
        rudderYaw.localEulerAngles = Vector3.up * yawVelocity / maxTurnSpeed * rudderAngle;
        rudderPitch.localEulerAngles = Vector3.left * pitchVelocity / maxPitchSpeed * rudderAngle;

        physicalContainer.localEulerAngles=Vector3.forward * (yawVelocity * -0.75f);
/*
        propeller.Rotate (Vector3.forward * Time.deltaTime * propellerSpeedFac * speedPercent, Space.Self);
        propSpinMat.color = new Color (propSpinMat.color.r, propSpinMat.color.g, propSpinMat.color.b, speedPercent * .3f);
        */

        float pitch=transform.localEulerAngles.x;
        
        if (pitch>180) pitch-=360.0f;
        if (pitch>pitchMax) {
            transform.localEulerAngles+=(Vector3.left*(pitch-pitchMax));
            pitchVelocity=0.0f;
        } else if (pitch<-pitchMax) {
            transform.localEulerAngles+=(Vector3.left*(pitch+pitchMax));
            pitchVelocity=0.0f;
        }

        float altitude=this.transform.position.y;

        //float dCenter=0f;
        //debugText.text="impact="+dCenter.ToString("00")+" rudder="+densityTurnHorizontal.ToString(".00")+" "+" elevator="+densityTurnVertical.ToString(".00")+" pitch="+pitch.ToString("000")+" time="+Time.fixedTime.ToString("000")+" altitude="+altitude.ToString("000.0");

         debugText.text=autopilot.debugString;
        
        //debugText.text="yaw="+transform.localEulerAngles.y.ToString("0.00")+" pitch="+transform.localEulerAngles.x.ToString("0.00")+" roll="+transform.localEulerAngles.z.ToString("0.00");
    }
}