﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaterFollow : MonoBehaviour
{
    public Transform ToFollow;
    public float Height;

    public float CenterOffset=40;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        Vector3 pOffset= ToFollow.TransformPoint(new Vector3(0,0,CenterOffset));
        pOffset.y=Height;
        transform.position=pOffset;
    }
}
