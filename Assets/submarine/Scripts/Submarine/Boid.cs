using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Boid : MonoBehaviour
{
    private Rigidbody rigidBody;
    private BoidGroup parentGroup;
    private Autopilot autopilot;

	[Header ("Autopilot")]
	public float autopilotClearanceRadius = 0.7f;
    public float autopilotVisionDistance = 16f;
    public float autopilotFOV = 22.5f;

    [Header ("Flight Controller")]

    public float pitchMax=60.0f;
    public float maxPitchSpeed = 200;
    public float maxTurnSpeed = 200;

    public float smoothSpeed = 3;
    public float smoothTurnSpeed = 1.5f;

	float yawVelocity=0;
    float pitchVelocity=0;
    private float currentSpeed=5;

    public void Init(Vector3 start, Quaternion rotation, BoidGroup parent) {
        this.parentGroup=parent;
        this.gameObject.layer=parentGroup.gameObject.layer;
        transform.position=start; 
        transform.rotation=rotation;
        Vector3 lea=transform.localEulerAngles;
        lea.z=0; // remove roll
        transform.localEulerAngles=lea;
		//transform.localScale=new Vector3(0.01f,0.01f,0.01f);

        //rigidBody=gameObject.AddComponent<Rigidbody>();
        //rigidBody.useGravity=false;
        //rigidBody.mass=0.01f;
        /*
        GameObject sphere = GameObject.CreatePrimitive(PrimitiveType.Sphere);
        sphere.transform.parent = this.gameObject.transform;
        sphere.layer=this.gameObject.layer;
        sphere.transform.localPosition = new Vector3(0,0,0);
        sphere.transform.localScale = new Vector3(0.5f,0.5f,0.5f);
        */
        /*
        Mesh holderMesh = new Mesh();
        ObjImporter newMesh = new ObjImporter();
        holderMesh = newMesh.ImportFile("submarine.Boid1.obj");

        MeshRenderer renderer = gameObject.AddComponent<MeshRenderer>();
        MeshFilter filter = gameObject.AddComponent<MeshFilter>();
        filter.mesh = holderMesh;
        */

        //var importer=AssetImporter.GetAtPath("submarine.Boid1.obj");

    }

    void Start () {
        autopilot=new Autopilot(transform,parentGroup.densityMesh);
		autopilot.clearanceRadius=this.autopilotClearanceRadius;
        autopilot.visionDistance=this.autopilotVisionDistance;
        autopilot.visionFOV=this.autopilotFOV;
    }

    public void OnCollisionEnter(Collision collision) {
        Debug.Log("OnCollisionEnter");
        parentGroup.DestroyBoid(this);
    }

    void Update() {
        //check OOB
        if (parentGroup!=null && parentGroup.isOOB(this)) {
            Debug.Log("OOB");
            parentGroup.DestroyBoid(this);
        }

        autopilot.process();

        float v= autopilot.getTurnVertical();
        float targetPitchVelocity = v * maxPitchSpeed;
        pitchVelocity = Mathf.Lerp (pitchVelocity, targetPitchVelocity, Time.deltaTime * smoothTurnSpeed);
        
        float h=autopilot.getTurnHorizontal();
        float targetYawVelocity = h * maxTurnSpeed;
        yawVelocity = Mathf.Lerp (yawVelocity, targetYawVelocity, Time.deltaTime * smoothTurnSpeed);

        transform.localEulerAngles += (Vector3.up * yawVelocity + Vector3.left * pitchVelocity) * Time.deltaTime;
        transform.Translate (transform.forward * currentSpeed * Time.deltaTime, Space.World);
    }
}