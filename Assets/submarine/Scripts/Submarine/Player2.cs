﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.InputSystem;

public class Player2 : MonoBehaviour
{
    [Header ("Autopilot")]
    public bool autopilotEnabled = true;
    public float autopilotClearanceRadius = 0.8f;
    public float autopilotVisionDistance = 32f;
    public float autopilotFOV = 22.5f;

    [Header ("Flight Controller")]
    // Maximum speed in metres / second.
    public float maxSpeed = 10f;
    //Power output of engine to effect speed
    public float enginePower=10f;    
    // Turning force as a multiple of the thrust force.
    public float turnForceMultiplier = 1.0f;

    [Header ("General")]

    
    public Text debugText;

    public BulletGroup bulletGroup;

    public Transform physicalContainer;
    
    private Rigidbody rigidBody;
    private Autopilot autopilot;

    public InputAbstraction inputAbstraction;

    // Start is called before the first frame update
    void Start()
    {
        rigidBody=GetComponent<Rigidbody>();
        autopilot=new Autopilot(transform);
        updateAutopilot();
    }

    void OnValidate() {
        updateAutopilot();
    }

    void updateAutopilot() {
        if (autopilot==null) return;
        autopilot.clearanceRadius=this.autopilotClearanceRadius;
        autopilot.visionDistance=this.autopilotVisionDistance;
        autopilot.visionFOV=this.autopilotFOV;
    }

    // Update is called once per frame
    void Update() {
        if (autopilotEnabled) {
            autopilot.process();
        }

        if (inputAbstraction.getFire()) {
            bulletGroup.fire(transform.position+transform.forward*1.0f,transform.forward);
        }
    }

    void FixedUpdate()
    {   
        
        // Apply force to accelerate.
        float powerRequest=inputAbstraction.getAcceleration();
        
        float speed=rigidBody.velocity.magnitude;
        float desiredSpeed=Mathf.Clamp(speed+enginePower*powerRequest,0.0001f,maxSpeed);
        float speedDiff = rigidBody.velocity.magnitude - desiredSpeed;
        Vector3 accelerationForce = transform.forward * speedDiff;
        rigidBody.AddForce(-accelerationForce, ForceMode.Acceleration);

        float v=inputAbstraction.getYMovement();
        float h=inputAbstraction.getXMovement();
        Vector3 controlForce = new Vector3(
            h * turnForceMultiplier * speed,
            v * turnForceMultiplier * speed,
            0.0f
        );
        rigidBody.AddRelativeForce(controlForce, ForceMode.Force);

        // Rotate the aircraft to face in the direction that it is flying in.
        if (rigidBody.velocity.magnitude>0.01f) {
            Vector3 up=transform.up;
            Vector3 directionOfMotion=rigidBody.velocity.normalized;
            Quaternion rot=Quaternion.LookRotation(directionOfMotion,up);
            transform.rotation = Quaternion.Slerp(transform.rotation,rot,0.1f);
            //transform.forward=rigidBody.velocity.normalized;
        }
        
        //rotate up direction to align with world only when nearly level and not controlling pitch
        // this allows loops but always self rights
        
        if (transform.forward.y<0.5f && Mathf.Abs(v)<0.25f) {
            Vector3 forward=transform.forward;
            Vector3 localUp=transform.up;
            Vector3 worldUp=Vector3.up;
            Vector3 newUp=Vector3.Slerp(localUp,worldUp,0.01f);
            transform.rotation = Quaternion.LookRotation(forward,newUp);
        }
        

        //roll body according to turning
        float roll=physicalContainer.localEulerAngles.z;
        if (roll>180) roll-=360.0f;
        float desiredRoll=h * turnForceMultiplier * -45f;
        roll=Mathf.Lerp(roll,desiredRoll,0.1f);
        physicalContainer.localEulerAngles=Vector3.forward * roll;
    }
}
