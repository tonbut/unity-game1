﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletGroup : MonoBehaviour
{
    public MeshGenerator densityMesh;

    public void fire(Vector3 start, Vector3 direction) {
        GameObject bullet = new GameObject ("Bullet");
        bullet.transform.parent = this.transform;
        bullet.layer=this.gameObject.layer;
        Bullet bulletScript = bullet.AddComponent<Bullet> ();
        bulletScript.Init(start+direction*3.0f,direction,this);
    }

    void OnDestroy () {
        var bullets = FindObjectsOfType<Bullet> ();
        for (int i = bullets.Length - 1; i >= 0; i--) {
            Destroy (bullets[i].gameObject);
        }
    }

    public void DestroyBullet(Bullet b) {
        Destroy (b.gameObject);
    }

    public bool isOOB(Bullet b) {
        if (densityMesh!=null) {
            return !densityMesh.OnMesh(b.gameObject.transform.position);
        } else {
            return false;
        }
    }
}

public class Bullet : MonoBehaviour
{
    private Rigidbody rigidBody;
    private BulletGroup parentGroup;

    public void Init(Vector3 start, Vector3 direction, BulletGroup parent) {
        parentGroup=parent;
        transform.position=start; 
        transform.forward=direction;
        transform.localScale = new Vector3(0.125f,0.125f,4.0f);
        rigidBody=gameObject.AddComponent<Rigidbody>();
        rigidBody.useGravity=false;
        rigidBody.mass=0.01f;
        rigidBody.AddRelativeForce(Vector3.forward*20f, ForceMode.VelocityChange);

        GameObject sphere = GameObject.CreatePrimitive(PrimitiveType.Sphere);
        sphere.transform.parent = this.gameObject.transform;
        sphere.layer=this.gameObject.layer;
        sphere.transform.localPosition = new Vector3(0,0,0);
        //sphere.transform.localScale = new Vector3(0.25f,0.25f,1.0f);
        //sphere.transform.rotation
    }

    public void OnCollisionEnter(Collision collision) {
        parentGroup.DestroyBullet(this);
    }

    void Update() {
        //check OOB
        if (parentGroup.isOOB(this)) {
            parentGroup.DestroyBullet(this);
        }
    }
}
