﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoidGroup : MonoBehaviour
{
    public MeshGenerator densityMesh;
    public Transform player;
    //public MeshFilter mesh;
    public Boid prefab;
    public CamFollow camera;

    public int numberOf=1;

    // Update is called once per frame
    void Update()
    {
        int existingBoidCount=transform.childCount;
        if (existingBoidCount<numberOf) {
            CreateBoid();
        }
    }

    void CreateBoid() {
        // generate random location and test it's valid
        bool valid=false;
        float range=20.0f;
        Vector3 start;
        int count=0;
        do {
            start=player.position+new Vector3(Random.value*range*2-range,0,Random.value*range*2-range);
            if (count++>5) break;
            valid=true;
        } while (!valid);

        Boid boid = Instantiate (prefab);
        boid.transform.parent = transform;
        boid.Init(start,Random.rotation,this);
        
        //camera.target=boid.transform;
    }

    void OnDestroy () {
        var boids = FindObjectsOfType<Boid> ();
        for (int i = boids.Length - 1; i >= 0; i--) {
            Destroy (boids[i].gameObject);
        }
    }

    public void DestroyBoid(Boid b) {
        Destroy (b.gameObject);
    }

    public bool isOOB(Boid b) {
        if (densityMesh!=null) {
            return !densityMesh.OnMesh(b.gameObject.transform.position);
        } else {
            return false;
        }
    }
}

/*
public class Boid : MonoBehaviour
{
    private Rigidbody rigidBody;
    private BoidGroup parentGroup;
    private Autopilot autopilot;

    float yawVelocity=0;
    float pitchVelocity=0;
    private float currentSpeed=5;

    public float pitchMax=60.0f;
    public float maxPitchSpeed = 200;
    public float maxTurnSpeed = 200;

    public float smoothSpeed = 3;
    public float smoothTurnSpeed = 1.5f;

    public void Init(Vector3 start, Quaternion rotation, BoidGroup parent) {
        parentGroup=parent;
        transform.position=start; 
        transform.rotation=rotation;


        var importer=AssetImporter.GetAtPath("submarine.Boid1.obj");

    }

    void Start () {
        autopilot=new Autopilot(transform,parentGroup.densityMesh);
    }

    public void OnCollisionEnter(Collision collision) {
        parentGroup.DestroyBoid(this);
    }

    void Update() {
        //check OOB
        if (parentGroup.isOOB(this)) {
            parentGroup.DestroyBoid(this);
        }

         autopilot.process();

        float v= autopilot.getTurnVertical();
        float targetPitchVelocity = v * maxPitchSpeed;
        pitchVelocity = Mathf.Lerp (pitchVelocity, targetPitchVelocity, Time.deltaTime * smoothTurnSpeed);
        
        float h=autopilot.getTurnHorizontal();
        float targetYawVelocity = h * maxTurnSpeed;
        yawVelocity = Mathf.Lerp (yawVelocity, targetYawVelocity, Time.deltaTime * smoothTurnSpeed);

        transform.localEulerAngles += (Vector3.up * yawVelocity + Vector3.left * pitchVelocity) * Time.deltaTime;
        transform.Translate (transform.forward * currentSpeed * Time.deltaTime, Space.World);
    }
}
*/
