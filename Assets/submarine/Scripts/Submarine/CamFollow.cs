﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CamFollow : MonoBehaviour
{
    public Transform target;
    public Vector3 followOffset;
    public float lookAheadDst = 10;
    public float smoothTime = .1f;
    public float rotSmoothSpeed = 3;
    public float camRotateSpeed = 90f;
    public LayerMask camOcclusion;                //the layers that will be affected by collision

    public InputAbstraction inputAbstraction;

    private Vector3 smoothV;
    private float rotateAround=-45f;
    
    void FixedUpdate()
    {
        /*
        if (Input.GetKey (KeyCode.Z)) {
            rotateAround += camRotateSpeed * Time.deltaTime;
        }
        if (Input.GetKey (KeyCode.X)) {
            rotateAround -= camRotateSpeed * Time.deltaTime;
        }
        */

        float h=inputAbstraction.getXCameraRotate();
        rotateAround += h * camRotateSpeed * Time.deltaTime;

        if(rotateAround > 360) {
            rotateAround = 0f;
        }
        else if(rotateAround < 0f) {
            rotateAround = (rotateAround + 360f);
        }

        /*
        Vector3 targetPos = target.position
            + target.up * followOffset.y
            + target.forward * followOffset.z
            + target.right * followOffset.x;
        */

        Vector3 offset=new Vector3(0,followOffset.y,0)+Quaternion.Euler(0,rotateAround,0)*Vector3.one*followOffset.z;
    
        Vector3 targetPos = target.transform.TransformPoint(offset);

        
        targetPos=occludeRay(transform.position,targetPos,0.5f);
        
        transform.position = Vector3.SmoothDamp(transform.position, targetPos, ref smoothV, smoothTime);

        Quaternion rot = transform.rotation;
        transform.LookAt(target.position + target.forward * lookAheadDst);
        Quaternion targetRot = transform.rotation;

        transform.rotation = Quaternion.Slerp(rot,targetRot,Time.deltaTime * rotSmoothSpeed);
    }

    Vector3 occludeRay( Vector3 from, Vector3 to, float clearance ) {
        Vector3 result;
        RaycastHit wallHit = new RaycastHit();
        //Vector3 dir=(to-from).normalized;
        //float length=(to-from).magnitude;
        if(Physics.Linecast(from, to, out wallHit, camOcclusion)) {
        //if(Physics.SphereCast(from, clearance, dir, out wallHit, length, camOcclusion)) {
            result = wallHit.point+ wallHit.normal * clearance;
        }
        else {
            result=to;
        }
        return result;
    }
}
