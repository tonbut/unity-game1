using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

public class EnemyBot : MonoBehaviour {

	[Header ("Autopilot")]
    public float densityThreshold = 3.0f;
    public MeshGenerator densityMesh;

	public Vector3 startPosition;
	[Header ("General")]
	

    public float pitchMax=60.0f;
    public float maxPitchSpeed = 50;
    public float maxTurnSpeed = 80;

    public float smoothSpeed = 3;
    public float smoothTurnSpeed = 1.5f;

	private float currentSpeed=3;
	private Autopilot autopilot;

	private Vector3 velocity;
	float yawVelocity;
    float pitchVelocity;

	void Start () {
        autopilot=new Autopilot(transform,densityMesh);
    }

	void Update () {

        if (densityMesh??false) {
            autopilot.process();
		}

		if (autopilot.isOutOfBounds()) {
			transform.position=startPosition;
			autopilot.restart();
		}

        float v= autopilot.getTurnVertical();
        float targetPitchVelocity = v * maxPitchSpeed;
        pitchVelocity = Mathf.Lerp (pitchVelocity, targetPitchVelocity, Time.deltaTime * smoothTurnSpeed);
        
        float h=autopilot.getTurnHorizontal();
        float targetYawVelocity = h * maxTurnSpeed;
        yawVelocity = Mathf.Lerp (yawVelocity, targetYawVelocity, Time.deltaTime * smoothTurnSpeed);

        transform.localEulerAngles += (Vector3.up * yawVelocity + Vector3.left * pitchVelocity) * Time.deltaTime;
        transform.Translate (transform.forward * currentSpeed * Time.deltaTime, Space.World);

		//for roll
        //physicalContainer.localEulerAngles=Vector3.forward * (yawVelocity * -0.75f);

	}
}