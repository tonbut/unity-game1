# Unity Game 1

A single scene game with a flying craft with in a procedural generated cave system. This game is developed
using [Unity](https://unity.com) using the [High Definition Rendering Pipeline](https://docs.unity3d.com/Packages/com.unity.render-pipelines.high-definition@7.1/manual/Getting-started-with-HDRP.html). The code uses several Compute Shaders to generate the procedural world using [3d voronoi noise(https://en.wikipedia.org/wiki/Worley_noise) and [Marching Cubes algorithm](https://en.wikipedia.org/wiki/Marching_cubes) to create a threshold surface from the noise function.

The codebase started out from exploring and decomposing a nice [demo by Sebastian Lague](https://www.youtube.com/watch?v=M3iI2l0ltbE). Once I understood compute shaders and some of the basic principles of Unity I created some autonomous boids to fly around the world and the a keyboard and gamepad controllable play. There isn't much gameplay at the moment but you can shoot and hit the boids as well as exploring the infinite landscape. At the bottom level the cave system has reflective water and higher up the caves eventually give way to the surface where rolling hills lie with the occasional openings back into the caves below.

Control keys: 
- Q/A accelerate/decelerate
- cursor keys steer up/down/left/right
- [space] fire
- also PS3 gamepad configured using usual controls as well as left joystick to rotate camera

Some [youtube videos here](https://www.youtube.com/watch?v=_FftXt_txZk&t=1s).

![screenshot 1](readme_images/screenshot1.jpg)
![screenshot 2](readme_images/screenshot2.jpg)
![screenshot 3](readme_images/screenshot3.jpg)
